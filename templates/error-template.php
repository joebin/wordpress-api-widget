<div class="wapiw-error">
    <h2>WAPIW Error</h2>
    <h3 class="status-code">
        Status: <span><?php echo $data['status'] ?></span>
    </h3>
    <p class="message">
        <?php echo $data['message'] ?>
    </p>
</div>

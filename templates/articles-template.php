<div class="wapiw-articles">
    <?php foreach($data as $article) : ?>
        <article data-id="<?php echo $article->id ?>">
            <header>
                <h2 class="article-title"><?php echo $article->title ?></h2>
            </header>
            <section class="article-content">
                <?php echo $article->content ?>
            </section>
        </article>
    <?php endforeach; ?>
</div>


<?php
/**
 * Plugin Name: Wordpress API widget
 * Description: Fetches articles from the article API.
 * Version: 0.0.1
 * Author:
 */

/**
 * Exit if accessed directly.
 */
if (!defined('ABSPATH')) {
    exit;
}

define('WORDPRESS_API_WIDGET_PATH', plugin_dir_path(__FILE__));
define('WORDPRESS_API_WIDGET_URI', plugins_url('', __FILE__));

/**
 * API, can be set in wp-config.
 */
if (!defined('WAPIW_API')) {
    define('WAPIW_API', 'http://localhost:8000/');
}

/**
 * Load vendors
 */
if (file_exists(WORDPRESS_API_WIDGET_PATH . '/vendor/autoload.php')) {
    require_once WORDPRESS_API_WIDGET_PATH . '/vendor/autoload.php';
}

/**
 * Load a template file.
 * @param $template
 * @param $context
 * @return string
 */
function wapiw_load_template($template, $context)
{
    $data = $context; // Context data for templates.

    ob_start();

    // Check if theme has overridden template files
    if (file_exists(TEMPLATEPATH . "/templates/wapiw/$template")) {
        require(TEMPLATEPATH . "/templates/wapiw/$template");
    } elseif (file_exists(WORDPRESS_API_WIDGET_PATH . "/templates/$template")) {
        require(WORDPRESS_API_WIDGET_PATH . "/templates/$template");
    }

    return ob_get_clean();
}

/**
 * Get all items from a model.
 * @param $model
 * @return array|bool|mixed|object
 */
function wapiw_get_all($model)
{
    try {
        $client = new GuzzleHttp\Client([
            'base_uri' => WAPIW_API,
            'http_errors' => false,
        ]);
        $response = $client->request('GET', $model);
    } catch (GuzzleHttp\Exception\ConnectException $e) {
        $template = wapiw_load_template("error-template.php", [
            'status' => 'API Error',
            'message' => 'WAPIW cannot connect to API.'
        ]);
        return [
            'error' => true,
            'template' => $template
        ];
    }
    if ($response->getStatusCode() != 200) {
        $template = wapiw_load_template("error-template.php", [
            'status' => $response->getStatusCode(),
            'message' => 'Wops! Something went wrong.'
        ]);
        return [
            'error' => true,
            'template' => $template
        ];
    }

    return json_decode($response->getBody());
}

/**
 * Get a single item from a model. Not yet implemented.
 * @param $model
 * @param $id
 * @return array
 */
function wapiw_get_single($model, $id)
{
    $template = wapiw_load_template("error-template.php", [
        'status' => 'NYI',
        'message' => 'wapiw_get_single is not implemented yet.'
    ]);
    return [
        'error' => true,
        'template' => $template
    ];
}

/**
 * Shortcode to display results from API request.
 * @param $attributes
 * @return string
 */
function wapiw_shortcode($attributes)
{
    $attributes = shortcode_atts([
        'model' => 'articles',
        'id' => '0'
    ], $attributes, 'wapiw');

    if ($attributes['id'] == '0') { // Get all
        $data = wapiw_get_all($attributes['model']);
    } else { // Requesting specific id
        $data = wapiw_get_single($attributes['model'], $attributes['id']);
    }

    if (isset($data['error']) && $data['error'] === true) { // If data is an error
        return $data['template'];
    } else {
        return wapiw_load_template("{$attributes['model']}-template.php", $data);
    }
}

add_shortcode('wapiw', 'wapiw_shortcode');

# Wordpress API widget #

This is a plugin used to display data from the developer test API

### Install ###

Once the plugin is enabled a url for the API needs to be configured. Default is:

```
http://localhost:8000/
```

To override add this to your wp-config.php file:

```
define('WAPIW_API', '[API URL HERE]');
```

### Usage ###

To display data from the API you can use simple shortcodes. Example:

```
[wapiw model="articles"]
```

This will fetch and display all articles where the shortcode is used. If model is omitted it defaults to articles. An id 
can optionally be added to retrieve only one item, though this functionality is currently not yet implemented.

### Developer ###

It is possible to override the template files of the plugin in your theme. Simply copy all files in 
`wordpress-api-widget/templates` to `your-theme/templates/wapiw`